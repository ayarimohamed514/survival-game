"use strict"

import Phaser from 'phaser'
import Player from './Player'
import Resource from './Resource'

function MainScene() {
  Phaser.Scene.call(this, 'MainScene')
}
MainScene.prototype = Object.create(Phaser.Scene.prototype)
MainScene.prototype.constructor = MainScene

MainScene.prototype.preload = function() {
  Player.preload(this)
  Resource.preload(this)
  this.load.image('tiles', 'assets/map/RPG Nature Tileset.png')
  this.load.tilemapTiledJSON('map', 'assets/map/survival_meh.json')

}

MainScene.prototype.create = function() {
  const map = this.make.tilemap({ key: 'map' })
  this.map = map
  const tileset = map.addTilesetImage('RPG Nature Tileset', 'tiles', 32, 32, 0, 0)
  this.tileset = tileset

  const layer1 = map.createLayer("Tile Layer 1", tileset, 0, 0)
  const layer2 = map.createLayer("Tile Layer 2", tileset, 0, 0)

  layer1.setCollisionByProperty({ collides: true })
  this.matter.world.convertTilemapLayer(layer1)

  this.addResources()

  this.player = new Player({ scene: this, x: 100, y: 100, texture: 'female', frame: 'townsfolk_f_idle_1' })
  this.add.existing(this.player)
  //  this.player = new Phaser.Physics.Matter.Sprite(this.matter.world)

  this.player.inputKeys = this.input.keyboard.addKeys({
    up: Phaser.Input.Keyboard.KeyCodes.W,
    down: Phaser.Input.Keyboard.KeyCodes.S,
    left: Phaser.Input.Keyboard.KeyCodes.A,
    right: Phaser.Input.Keyboard.KeyCodes.D
  })
}

MainScene.prototype.update = function() {
  this.player.update()
}

MainScene.prototype.addResources = function() {
  const resources = this.map.getObjectLayer('Resources')

  resources.objects.forEach(resource => {
    let item = new Resource({ scene: this, tileset: this.tileset, resource})
  })
}

export default MainScene
