import Phaser from 'phaser'

function Player(data) {
  let { scene, x, y, texture, frame } = data
  Phaser.Physics.Matter.Sprite.call(this, scene.matter.world, x, y, texture, frame)
  this.scene.add.existing(this)
  this.touching = []

  // Weapon
  this.spriteWeapon = new Phaser.GameObjects.Sprite(this.scene, 0, 0, 'items', 162)
  this.spriteWeapon.setOrigin(0.25, 0.75)
  this.spriteWeapon.setScale(0.8)
  this.scene.add.existing(this.spriteWeapon)

  const { Body, Bodies } = Phaser.Physics.Matter.Matter
  const playerCollider = Bodies.circle(this.x, this.y, 12, { isSensoer: false, label: 'playerCollider' })
  const playerSensor = Bodies.circle(this.x, this.y, 24, { isSensor: true, lable: 'playerSensor' })

  const compoundBody = Body.create({
    parts: [playerCollider, playerSensor],
    frictionAir: 0.35
  })

  this.setExistingBody(compoundBody)
  this.setFixedRotation()

  this.createMiningCollisions(playerSensor)
  this.scene.input.on('pointermove', (pointer) => this.setFlipX(pointer.worldX < this.x))
}
Player.prototype = Object.create(Phaser.Physics.Matter.Sprite.prototype)
Player.prototype.constructor = Player

Player.preload = function(scene) {
  console.log('preload')
  scene.load.atlas('female', 'assets/images/female.png', 'assets/images/female_atlas.json')
  scene.load.animation('female_anim', 'assets/images/female_anim.json')
  scene.load.spritesheet('items', 'assets/images/shikashi_fantasy_icons/1-TransparentIcons.png', {
    frameWidth: 32,
    frameHeight: 32
  })
}

Player.prototype.update = function() {
  const speed = 2.5
  const playerVelocity = new Phaser.Math.Vector2()

  if (this.inputKeys.left.isDown) {
    playerVelocity.x = -1
  } else if (this.inputKeys.right.isDown) {
    playerVelocity.x = 1
  }
  if (this.inputKeys.up.isDown) {
    playerVelocity.y = -1
  } else if (this.inputKeys.down.isDown) {
    playerVelocity.y = 1
  }
  playerVelocity.normalize()
  playerVelocity.scale(speed)
  this.setVelocity(playerVelocity.x, playerVelocity.y)

  if (Math.abs(this.body.velocity.x) > 0.1 || Math.abs(this.body.velocity.y) > 0.1) {
    this.anims.play('female_walk', true)
  } else {
    this.anims.play('female_idle', true)
  }
  this.spriteWeapon.setPosition(this.x, this.y)
  this.weaponRotate()
}

Player.prototype.weaponRotate = function() {
  const pointer = this.scene.input.activePointer
  if (pointer.isDown) {
    this.weaponRotation += 6
  } else {
    this.weaponRotation = 0
  }
  if (this.weaponRotation > 100) {
    this.whackStuff()
    this.weaponRotation = 0
  }

  if (this.flipX) {
    this.spriteWeapon.setAngle(-this.weaponRotation - 90)
  } else {
    this.spriteWeapon.setAngle(this.weaponRotation)
  }
}

Player.prototype.createMiningCollisions = function(playerSensor) {
  this.scene.matterCollision.addOnCollideStart({
    objectA: [playerSensor],
    callback: other => {
      if (other.bodyB.isSensor) { return }
      this.touching.push(other.gameObjectB)
      console.log(this.touching.length, other.gameObjectB.name)
    },
    context: this.scene
  })

  this.scene.matterCollision.addOnCollideEnd({
    objectA: [playerSensor],
    callback: other => {
      if (other.bodyB.isSensor) { return }
      this.touching = this.touching.filter(gameObject => gameObject !== other.gameObjectB)
      console.log(this.touching.length)
    },
    context: this.scene
  })
}

Player.prototype.whackStuff = function() {
  this.touching = this.touching.filter(gameObject => gameObject.hit && !gameObject.dead)
  this.touching.forEach(gameObject => {
    gameObject.hit()
    if (gameObject.dead) { gameObject.destroy() }
  })
}

export default Player
