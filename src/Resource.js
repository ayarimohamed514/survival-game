import Phaser from "phaser"

function Resource(data) {
  const { scene, resource, tileset } = data
  const { type: resourceType } = tileset.getTileData(resource.gid)
  const { yOrigin } = tileset.getTileProperties(resource.gid)

  Phaser.Physics.Matter.Sprite.call(this, scene.matter.world, resource.x, resource.y, 'resources', resourceType.toLowerCase())
  scene.add.existing(this)

  this.name = resourceType.toLowerCase()
  this.sound = this.scene.sound.add(this.name)
  this.x += this.width / 2
  this.y -= this.height / 2
  this.y = this.y + this.height * (yOrigin - 0.5)
  const { Body, Bodies } = Phaser.Physics.Matter.Matter
  const circleCollider = Bodies.circle(this.x, this.y, 12, { isSensor: false, label: 'collider' })

  this.setExistingBody(circleCollider)
  this.setOrigin(0.5, yOrigin)
  this.setStatic(true)
  this.health = 5

  Object.defineProperty(this, 'dead', {
    get() {
      return this.health <= 0
    }
  })
}
Resource.prototype = Object.create(Phaser.Physics.Matter.Sprite.prototype)
Resource.prototype.constructor = Resource

Resource.preload = function(scene) {
  scene.load.atlas('resources', 'assets/images/resources.png', 'assets/images/resources_atlas.json')
  scene.load.audio('tree', 'assets/audio/tree.mp3')
  scene.load.audio('rock', 'assets/audio/rock1.wav')
  scene.load.audio('bush', 'assets/audio/bush.wav')
}

Resource.prototype.hit = function() {
  if (this.sound) {
    this.sound.play()
  }
  this.health--
  console.log(`Hitting: ${this.name} Health: ${this.health}`)
}


export default Resource
